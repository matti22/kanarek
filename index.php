<?php

require_once 'Routing.php';

$path = trim($_SERVER['REQUEST_URI'], '/');
$path = parse_url($path, PHP_URL_PATH);

Routing::get('login', 'DefaultController');
Routing::get('registration', 'DefaultController');
Routing::get('profile', 'DefaultController');
Routing::get('aboutApp', 'DefaultController');
Routing::get('bugsRep', 'DefaultController');

Routing::post('login', 'SecurityController');
Routing::post('registration', 'SecurityController');

Routing::post('logout', 'HomepageController');
Routing::get('busStops', 'HomepageController');
Routing::post('search', 'HomepageController');
Routing::post('addEmergency', 'HomepageController');
Routing::get('homepage', 'HomepageController');

Routing::post('logout', 'ProfileController');
Routing::post('changeNickname', 'ProfileController');
Routing::post('changeEmail', 'ProfileController');
Routing::post('changePassword', 'ProfileController');

Routing::post('reportBug', 'ReportBugsController');
Routing::run($path);