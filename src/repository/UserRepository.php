<?php

require_once 'Repository.php';
require_once __DIR__.'/../models/User.php';

class UserRepository extends Repository{

    public function getUser(string $email): ?User{
        $stmt = $this->database->connect()->prepare('
            SELECT * FROM public.users WHERE email = :email
        ');
        $stmt->bindParam(':email', $email, PDO::PARAM_STR);
        $stmt->execute();

        $user = $stmt->fetch(PDO::FETCH_ASSOC);

        if($user == false){
            return null;
        }

        $cookie_value = json_encode($user);
        setcookie("logUser", $cookie_value, time() + (3600 * 24), "/");

        return new User(
            $user['email'],
            $user['password'],
            $user['nickname']
        );
    }

    public function getAllNicknames(){
        $stmt = $this->database->connect()->prepare('
            SELECT nickname FROM public.users
        ');
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getAllEmails(){
        $stmt = $this->database->connect()->prepare('
            SELECT email FROM public.users
        ');
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public function addUser(User $user){
        $stmt = $this->database->connect()->prepare('
            INSERT INTO public.users (email, password, nickname)
            VALUES (?, ?, ?)
        ');

        $stmt->execute([
           $user->getEmail(),
           $user->getPassword(),
           $user->getNickname()
        ]);
    }

    //TODO dodać aktualizowanie na bieżąco nie tylko po przelogowaniu
    public function setNewNickname(User $user, string $nickname){
        $stmt = $this->database->connect()->prepare('
            UPDATE public.users SET nickname = :nickname WHERE email = :email
        ');

        $stmt->bindParam(':nickname', $nickname, PDO::PARAM_STR);
        $email = $user->getEmail();
        $stmt->bindParam(':email', $email, PDO::PARAM_STR);
        $stmt->execute();
    }

    public function setNewEmail(User $user, string $email){
        $stmt = $this->database->connect()->prepare('
            UPDATE public.users SET email = :email WHERE nickname = :nickname
        ');

        $stmt->bindParam(':email', $email, PDO::PARAM_STR);
        $nickname = $user->getNickname();
        $stmt->bindParam(':nickname', $nickname, PDO::PARAM_STR);
        $stmt->execute();
    }

    public function setNewPassword(User $user, string $password){
        $stmt = $this->database->connect()->prepare('
            UPDATE public.users SET password = :password WHERE email = :email
        ');

        $password_hash = password_hash($password, PASSWORD_DEFAULT);
        $stmt->bindParam(':password', $password_hash, PDO::PARAM_STR);
        $email = $user->getEmail();
        $stmt->bindParam(':email', $email, PDO::PARAM_STR);
        $stmt->execute();
    }

}