<?php

require_once 'Repository.php';
require_once __DIR__.'/../models/BusStop.php';

class BusStopRepository extends Repository
{

    public function getBusStops(): array{
        $stmt = $this->database->connect()->prepare('
            SELECT * FROM public.bus_stops
        ');
        $stmt->execute();

        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getBusStopsObject(): array{
        $result = [];

        $stmt = $this->database->connect()->prepare('
            SELECT * FROM public.bus_stops
        ');
        $stmt->execute();

        $busStops = $stmt->fetchAll(PDO::FETCH_ASSOC);

        foreach ($busStops as $busStop){
            $result[] = new BusStop(
                $busStop['id'],
                $busStop['title'],
                $busStop['coordinates']
            );
        }
        return $result;
    }

    public function getBusStopsByTitle(string $searchString){
        $searchString = '%'.strtolower($searchString).'%';

        $stmt = $this->database->connect()->prepare('
            SELECT * FROM public.bus_stops WHERE LOWER(title) LIKE :search
        ');
        $stmt->bindParam(':search', $searchString, PDO::PARAM_STR);
        $stmt->execute();

        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
}