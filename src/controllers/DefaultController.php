<?php

require_once 'AppController.php';

class DefaultController extends AppController {

    public function login(){
        $this->render('login');
    }

    public function registration(){
        $this->render('registration');
    }


    public function profile(){
        if(!$_COOKIE['logUser']){
            $this->render('login');
        }else{
            $this->render('profile');
        }
    }

    public function aboutApp(){
        if(!$_COOKIE['logUser']){
            $this->render('login');
        }else{
            $this->render('aboutApp');
        }
    }

    public function bugsRep(){
        if(!$_COOKIE['logUser']){
            $this->render('login');
        }else{
            $this->render('bugsRep');
        }
    }
}