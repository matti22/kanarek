<?php

require_once 'AppController.php';
require_once __DIR__.'/../models/User.php';

class ReportBugsController extends AppController{

    public function __construct()
    {
        parent::__construct();
    }

    public function reportBug()
    {
        $this->render('bugsRep');
    }
}