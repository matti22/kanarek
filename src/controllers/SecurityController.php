<?php

require_once 'AppController.php';
require_once __DIR__.'/../models/User.php';
require_once __DIR__.'/../repository/UserRepository.php';

class SecurityController extends AppController{

    private $userRepository;

    public function __construct()
    {
        parent::__construct();
        $this->userRepository = new UserRepository();
    }

    public function login()
    {
        if (!$this->isPost()) {
            return $this->render('login');
        }

        $email = $_POST["email"];
        $password = $_POST["password"];

        $user = $this->userRepository->getUser($email);

        if(!$user){
            return $this->render('login', ['messages' => ['Użytkownik z podanym adresem email nie istnieje!!']]);
        }

        if($user->getEmail() !== $email){
            return $this->render('login', ['messages' => ['Użytkownik z podanym adresem email nie istnieje!!']]);
        }

        if(!password_verify($password, $user->getPassword())){
            return $this->render('login', ['messages' => ['Nieprawidłowe hasło!!']]);
        }

        $url = "http://$_SERVER[HTTP_HOST]";
        header("Location: {$url}/homepage");
    }

    public function registration(){
        if (!$this->isPost()) {
            return $this->render('registration');
        }
        $email = $_POST['email'];
        $password = $_POST['password'];
        $passwordRepeat = $_POST['password-repeat'];
        $nickname = $_POST['nickname'];

        $hashedPassword = password_hash($password, PASSWORD_DEFAULT);
        $allNicknames = $this->userRepository->getAllNicknames();
        $allEmails =$this->userRepository->getAllEmails();

        if(!$nickname){
            return $this->render('registration', ['messages' => ['Należy podać nazwię użytkownika!!']]);
        }

        if(strlen($nickname) > 20){
            return $this->render('registration', ['messages' => ['Za długa nazwa użytkownika!!']]);
        }

        foreach ($allNicknames as $nicknameFromDB){
            if($nicknameFromDB['nickname'] == $nickname){
                return $this->render('registration', ['messages' => ['Podana nazwa użytkownika jest już zajęta!!']]);
            }
        }

        if(!$email){
            return $this->render('registration', ['messages' => ['Należy podać email!!']]);
        }

        foreach ($allEmails as $emailFromDB){
            if($emailFromDB['email'] == $email){
                return $this->render('registration', ['messages' => ['Podany email jest już zajęty!!']]);
            }
        }

        if(!$password){
            return $this->render('registration', ['messages' => ['Należy podać hasło!!']]);
        }

        if(strlen($password) < 8){
            return $this->render('registration', ['messages' => ['Hasło musi być dłuższe!!']]);
        }

        if(!$passwordRepeat){
            return $this->render('registration', ['messages' => ['Należy podać hasło!!']]);
        }

        if($password !== $passwordRepeat){
            return $this->render('registration', ['messages' => ['Nieprawidłowe hasło!!']]);
        }

        $user = new User($email , $hashedPassword, $nickname);

        $this->userRepository->addUser($user);

        return $this->render('login', ['messages' => ['Pomyślnie zarejestrowano!']]);
    }
}