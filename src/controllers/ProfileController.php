<?php

require_once 'AppController.php';
require_once __DIR__.'/../models/User.php';
require_once __DIR__.'/../repository/UserRepository.php';

class ProfileController extends AppController{

    private $userRepository;

    public function __construct()
    {
        parent::__construct();
        $this->userRepository = new UserRepository();
    }

    function logout(){
        if (!$this->isPost()) {
            return $this->render('profile');
        }

        setcookie("logUser",'', time() - 1, '/');
        return $this->render('login');
    }

    function changeNickname(){
        if (!$this->isPost()) {
            return $this->render('profile');
        }

        $user_array = json_decode($_COOKIE['logUser'], true);
        $nickname = $_POST['nicknameInput'];
        $user = new User($user_array['email'], $user_array['password'], $user_array['nickname']);
        $allNicknames = $this->userRepository->getAllNicknames();

        if(!$nickname){
            return $this->render('profile', ['messages' => ['Pole nie może być puste!!']]);
        }

        if(strlen($nickname) > 20){
            return $this->render('profile', ['messages' => ['Za długa nazwa użytkownika (max 20 znaków)!!']]);
        }

        foreach ($allNicknames as $nicknameFromDB){
            if($user->getNickname() ===  $nickname) {
                return $this->render('profile');
            }
            if($nicknameFromDB['nickname'] == $nickname){
                return $this->render('profile', ['messages' => ['Podana nazwa użytkownika jest już zajęta!!']]);
            }
        }

        $this->userRepository->setNewNickname($user, $nickname);
        return $this->render('profile');
    }

    function changeEmail(){
        if (!$this->isPost()) {
            return $this->render('profile');
        }

        $user_array = json_decode($_COOKIE['logUser'], true);
        $email = $_POST['emailInput'];
        $user = new User($user_array['email'], $user_array['password'], $user_array['nickname']);
        $allEmails =$this->userRepository->getAllEmails();

        if(!$email){
            return $this->render('profile', ['messages' => ['Pole nie może być puste!!']]);
        }

        foreach ($allEmails as $emailFromDB){
            if($user->getEmail() ===  $email) {
                return $this->render('profile');
            }
            if($emailFromDB['email'] == $email){
                return $this->render('profile', ['messages' => ['Podany email jest już zajęty!!']]);
            }
        }

        $this->userRepository->setNewEmail($user, $email);
        return $this->render('profile');
    }

    function changePassword(){
        if (!$this->isPost()) {
            return $this->render('profile');
        }

        $user_array = json_decode($_COOKIE['logUser'], true);
        $password = $_POST['passwordInput'];
        $user = new User($user_array['email'], $user_array['password'], $user_array['nickname']);

        if(!$password){
            return $this->render('profile');
        }

        if(strlen($password) < 8){
            return $this->render('profile', ['messages' => ['Hasło musi być dłuższe!! (min 8 znaków)']]);
        }

        $this->userRepository->setNewPassword($user, $password);
        return $this->render('profile');
    }
}