<?php

require_once 'AppController.php';
require_once __DIR__.'/../repository/BusStopRepository.php';

class HomepageController extends AppController{

    private $busStopRepository;

    public function __construct()
    {
        parent::__construct();
        $this->busStopRepository = new BusStopRepository();
    }

    public function homepage(){
        if(!$_COOKIE['logUser']){
            $this->render('login');
        }else{
            $busStops = $this->busStopRepository->getBusStopsObject();
            $this->render('homepage', ['busStops' => $busStops]);
        }
    }

    function logout(){
        if (!$this->isPost()) {
            return $this->render('homepage');
        }

        setcookie("logUser",'', time() - 1, '/');
        return $this->render('login');
    }

    public function busStops(){
        header('Content-type: application/json');
        http_response_code(200);

        echo json_encode($this->busStopRepository->getBusStops());
    }

    public function search(){
        $contentType = isset($_SERVER["CONTENT_TYPE"]) ? trim($_SERVER["CONTENT_TYPE"]) : '';

        if($contentType === "application/json"){
            $content = trim(file_get_contents("php://input"));
            $decoded = json_decode($content, true);

            header('Content-type: application/json');
            http_response_code(200);

            echo json_encode($this->busStopRepository->getBusStopsByTitle($decoded['search']));
        }
    }

    public function addEmergency(){
        if (!$this->isPost()) {
            return $this->render('login');
        }
        $title = $_POST["search"];
        $areUniform = $_POST["areUniform"];
        $description = $_POST["description"];

    }
}