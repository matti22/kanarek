<?php

class BusStop
{
    private $title;
    private $coordinates;
    private $id;

    public function __construct(int $id ,string $title, string $coordinates)
    {
        $this->title = $title;
        $this->coordinates = $coordinates;
        $this->id = $id;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    public function getCoordinates(): string
    {
        return $this->coordinates;
    }

    public function setCoordinates(string $coordinates): void
    {
        $this->coordinates = $coordinates;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }



}