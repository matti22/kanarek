<!DOCTYPE html>

<head>
    <link rel="stylesheet" type="text/css" href="public/css/profile-style.css">
    <script src="https://kit.fontawesome.com/3c2b6a830a.js" crossorigin="anonymous"></script>
    <script type="text/javascript" src="./public/js/profileScript.js" defer></script>
    <title>PROFIL</title>
</head>

<body>
    <div class="base-container">
        <div class="logo">
            <img src="public/img/logo.png">
        </div>
        <div class="profile-panel">
            <div class="profile-image">
                <img src="public/img/user.png">
            </div>
            <form class="firstButton">
                <button type="button" class="profile-image-edit">Edytuj zdjęcie  .<i class="fas fa-edit"></i></button>
            </form>
            <ul>
                <li class="button">
                    <form name="nicknameForm" action="changeNickname" method="POST">
                        <div class="first-emoji">
                            <i class="far fa-laugh"></i>
                        </div>
                        <div class="text" id="nicknameOutput">
                            <?php
                            $user_array = json_decode($_COOKIE['logUser'], true);
                            echo $user_array['nickname'];
                            ?>
                        </div>
                        <input class="inputData" name="nicknameInput" type="text" value= <?php
                                echo $user_array['nickname'];
                            ?>>
                        <button type="button" class="second-emoji" id="nicknameButton" onclick="changeNickname()"><i class="fas fa-edit"></i></button>
                        <button type="submit" class="second-emoji" id="nicknameButtonSubmit"><i class="fas fa-edit"></i></button>
                    </form>
                </li>
                <li class="button">
                    <form name="emailForm" action="changeEmail" method="POST">
                        <div class="first-emoji">
                            <i class="far fa-envelope"></i>
                        </div>
                        <div class="text" id="emailOutput">
                            <?php
                                $user_array = json_decode($_COOKIE['logUser'], true);
                                echo $user_array['email'];
                            ?>
                        </div>
                        <input class="inputData" name="emailInput" type="text"value= <?php
                        echo $user_array['email'];
                        ?>>
                        <button type="button" class="second-emoji" id="emailButton" onclick="changeEmail()"><i class="fas fa-edit"></i></button>
                        <button type="submit" class="second-emoji" id="emailButtonSubmit"><i class="fas fa-edit"></i></button>
                    </form>
                </li>
                <li class="button">
                    <form name="passwordForm" action="changePassword" method="POST">
                        <div class="first-emoji">
                            <i class="fas fa-lock"></i>
                        </div>
                        <div class="text" id="passwordOutput">
                            <d>**************</d>
                        </div>
                        <input class="inputData" class="password" name="passwordInput" type="password" placeholder="**************">
                        <button type="button" class="second-emoji" id="passwordButton" onclick="changePassword()"><i class="fas fa-edit"></i></button>
                        <button type="submit" class="second-emoji" id="passwordButtonSubmit"><i class="fas fa-edit"></i></button>
                    </form>
                </li>
                <div class="message">
                    <?php
                    if(isset($messages)){
                        foreach ($messages as $message){
                            echo $message;
                        }
                    }
                    ?>
                </div>
                <form action="logout" method="POST">
                    <li class="logout">
                        <button href="login.php" class="outButton"><i class="fas fa-sign-out-alt"></i>Wyloguj</button>
                    </li>
                </form>
            </ul>
        </div>
        <div class="exit">
            <a href="homepage"><i class="fas fa-window-close"></i></a>
        </div>
    </div>
</body>