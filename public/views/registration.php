<!DOCTYPE html>

<head>
    <link rel="stylesheet" type="text/css" href="public/css/login-registration-style.css">
    <script type="text/javascript" src="./public/js/script.js" defer></script>
    <title>REGISTRATION PAGE</title>
</head>

<body>
    <div class="container">
        <div class="logo">
            <img src="public/img/logo.png">
        </div>
        <div class="registration-container">
            <form class="registration" action="registration" method="POST">
                <div class="message">
                    <?php
                    if(isset($messages)){
                        foreach ($messages as $message){
                            echo $message;
                        }
                    }
                    ?>
                </div>
                <input name="nickname" type="text" placeholder="nazwa użytkownika (max. 20 znaków)">
                <input name="email" type="text" placeholder="email">
                <input name="password" type="password" placeholder="hasło (min. 8 znaków)">
                <input name="password-repeat" type="password" placeholder="powtórz hasło">
                <a href="login">Powrót do strony logowania</a>
                <button name="registrationButton">Zarejestruj</button>
            </form>
        </div>
    </div>
</body>