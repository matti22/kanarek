<!DOCTYPE html>

<head>
    <link rel="stylesheet" type="text/css" href="public/css/aboutApp-bugs-style.css">
    <script src="https://kit.fontawesome.com/3c2b6a830a.js" crossorigin="anonymous"></script>
    <title>ABOUT APPLICATION</title>
</head>


<body>
<div class="base-container">
    <div class="main-container">
        <img src="public/img/logo.png">
        <div class="text-container">
            <d>Aplikacja Kanarek ma na celu zachęcanie urzytkowników komunikacji miejskiej do zakupu biletów oraz skasowaniu go na przystanku na którym dany użytkownik wsiadł. Wszystkie alerty o zwiększonym ryzyku kanarów mają na celu przypominanie o konsekwencjach jakie niesie ze sobą brak ważnego biletu.</d>
            <d>Wersja aplikacji: 0.2</d>
        </div>
    </div>
    <div class="exit">
        <a href="homepage"><i class="fas fa-window-close"></i></a>
    </div>
</div>
</body>