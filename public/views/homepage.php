<!DOCTYPE html>

<head>
    <link rel="stylesheet" type="text/css" href="public/css/homepage-style.css">
    <link rel="stylesheet" type="text/css" href="public/css/map-style.css">
    <script src="https://kit.fontawesome.com/3c2b6a830a.js" crossorigin="anonymous"></script>
    <script src='https://api.tiles.mapbox.com/mapbox-gl-js/v2.6.1/mapbox-gl.js'></script>
    <link href='https://api.tiles.mapbox.com/mapbox-gl-js/v2.6.1/mapbox-gl.css' rel='stylesheet'/>

    <script type="text/javascript" src="./public/js/mapScript.js" defer></script>
    <script type="text/javascript" src="./public/js/homepageScript.js" defer></script>
    <title>HOMEPAGE</title>
</head>

<body>
    <div class="base-container">
        <nav>
            <img src="public/img/logo.png">
            <div class="profile-image">
<!--                TODO dodać pobieranie zdjęcia profilowego z bazy danych-->
                <img src="public/img/user.png">
            </div>
            <div class="nickname">
                <?php
                $user_array = json_decode($_COOKIE['logUser'], true);
                echo $user_array['nickname'];
                ?>
            </div>
            <ul class="ul-nav">
                <li>
                    <a href="profile" class="button">
                        <i class="fas fa-user"></i> Profil
                    </a>
                </li>
                <li>
                    <a href="aboutApp" class="button">
                        <i class="fas fa-info-circle"></i> O aplikacji
                    </a>
                </li>
                <li>
                    <a href="bugsRep" class="button">
                        <i class="fas fa-flag"></i> Zgłoś błędy
                    </a>
                </li>
                <form action="logout" method="POST">
                    <li>
                        <button class="buttonLogOut" >
                            <i class="fas fa-sign-out-alt"></i> Wyloguj
                        </button>
                    </li>
                </form>
            </ul>
        </nav>
        <div class="map-box">
            <div id='map'></div>
            <div id="reportForm">
                <div class="reportPanel" id="reportPanel">
                    <div class="busStopsSearch">
                        <i class="fas fa-bus"></i>
                        <input class="search-bar" placeholder="Wpisz nazwę przystanku" name="search">
                        <i class="fas fa-times" id="clear-search" tabindex="2"></i>
                    </div>
                    <div class="searchResults" id="searchResults">
                        <?php foreach ($busStops as $busStop): ?>
                            <button type="button" id="<?=$busStop->getTitle()?>" onclick="setSearchbarUsingBusStopTitle(this.id)"><?=$busStop->getTitle()?></button>
                        <?php endforeach; ?>
                    </div>
                    <div class="uniformArea">
                        <div class="inputCheck">
                            <input type="checkbox" checked="checked" onclick="changeAreUniformView()" name="areUniform">
                        </div>
                        <div class="uniformPanel" id="areUniform">
                            <i class="fas fa-eye"></i>
                            Umundurowani
                        </div>
                        <div class="uniformPanel" id="areNotUniform">
                            <i class="fas fa-eye-slash"></i>
                            Nie Umundurowani
                        </div>
                    </div>
                    <textarea rows="23" placeholder="Opis zgłoszenia"  name="description"></textarea>
                    <div class="reportPanelButtonArea">
                        <button type="button" class="reportPanelButton" id="canselButton" onclick="hideReportArea()">
                            <i class="fas fa-times"></i>
                            Anuluj
                        </button>
                        <form action="addEmergency" method="POST">
                            <button type="submit" class="reportPanelButton" id="reportButton">
                                <i class="fas fa-long-arrow-alt-down"></i>
                                Zgłoś
                            </button>
                        </form>
                    </div>
                </div>
                <button type="button" class="buttonReport" id="mainReportButton" onclick="showReportArea()">
                    <i class="fas fa-arrow-circle-down"></i>
                    <d>Zgłoś</d>
                </button>
            </div>
            <div class="search-box">
                <input id="main-search" type="text" class="search-bar" placeholder="Szukaj przystanku">
                <i class="fas fa-times" id="clear-search" tabindex="2"></i>
            </div>
        </div>
        <div class="emergency-panel">
            <div class="emergency">
                <h2> <i class="fas fa-bus"></i> przystanek 5 <i class="fas fa-bus"></i> </h2>
                <d class="emergency-d"> <i class="fas fa-exclamation-triangle"></i>Zwiększone ryzyko kanara <i class="fas fa-exclamation-triangle"></i> </d>
                <div class="emergency-time">
                    <d class="emergency-d"> Zgłoszono: </d>
                    <output class="emergency-output"> 3 min temu </output>
                </div>
                <d class="emergency-d"> Nieumundurowani <i class="fas fa-eye-slash"></i> </d>
                <div class="emergency-message">
                    <output class="emergency-output"> Opis dodany przez osobę zgłaszającoą xyz</output>
                </div>
                <div class="confirmation">
                    <i class="fas fa-thumbs-up"></i>
                    <i class="fas fa-thumbs-down"></i>
                </div>
            </div>
        </div>
        </div>
    </div>
</body>

<template id="busStopsTemplate">
    <button type="button">title</button>
</template>
