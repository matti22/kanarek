<!DOCTYPE html>

<head>
    <link rel="stylesheet" type="text/css" href="public/css/aboutApp-bugs-style.css">
    <script src="https://kit.fontawesome.com/3c2b6a830a.js" crossorigin="anonymous"></script>
    <title>BUGS REPORT</title>
</head>

<body>
<div class="base-container">
    <div class="main-container">
        <img src="public/img/logo.png">
        <form class="text-container" action="reportBug" methot="POST">
            <textarea name="comment" rows="30" placeholder="Opisz błąd jaki chesz zgłosić... (max 500)"></textarea>
        </form>
        <button>Zgłoś błąd</button>
    </div>
    <div class="exit">
        <a href="homepage"><i class="fas fa-window-close"></i></a>
    </div>
</div>
</body>