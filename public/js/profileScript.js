const nicknameForm = document.querySelector('form[name="nicknameForm"]');
const nicknameButton = nicknameForm.querySelector('button[id="nicknameButton"]');
const nicknameButtonSubmit = nicknameForm.querySelector('button[id="nicknameButtonSubmit"]');
const nicknameInput = nicknameForm.querySelector('input[name="nicknameInput"]');
const nicknameOutput = nicknameForm.querySelector('div[id="nicknameOutput"]');

const emailForm = document.querySelector('form[name="emailForm"]');
const emailButton = emailForm.querySelector('button[id="emailButton"]');
const emailButtonSubmit = emailForm.querySelector('button[id="emailButtonSubmit"]');
const emailInput = emailForm.querySelector('input[name="emailInput"]');
const emailOutput = emailForm.querySelector('div[id="emailOutput"]');

const passwordForm = document.querySelector('form[name="passwordForm"]');
const passwordButton = passwordForm.querySelector('button[id="passwordButton"]');
const passwordButtonSubmit = passwordForm.querySelector('button[id="passwordButtonSubmit"]');
const passwordInput = passwordForm.querySelector('input[name="passwordInput"]');
const passwordOutput = passwordForm.querySelector('div[id="passwordOutput"]');

function changeView(element, condition){
    !condition ? element.style.cssText = 'display:none' : element.style.cssText = 'display:flex'
}

changeView(nicknameInput, false);
changeView(nicknameButtonSubmit, false);
changeView(emailInput, false);
changeView(emailButtonSubmit, false);
changeView(passwordInput, false);
changeView(passwordButtonSubmit, false);

function changeNickname(){
    changeView(nicknameInput, true);
    changeView(nicknameOutput, false);
    changeView(nicknameButtonSubmit, true);
    changeView(nicknameButton, false);
}

function changeEmail(){
    changeView(emailInput, true);
    changeView(emailOutput, false);
    changeView(emailButtonSubmit, true);
    changeView(emailButton, false);
}

function changePassword(){
    changeView(passwordInput, true);
    changeView(passwordOutput, false);
    changeView(passwordButtonSubmit, true);
    changeView(passwordButton, false);
}
