const areUniformView = document.querySelector('div[id="areUniform"]');
const areNotUniformView = document.querySelector('div[id="areNotUniform"]');
const reportPanel = document.querySelector('div[id="reportPanel"]');

const search = document.querySelector('input[name="search"]');
const searchResults = document.querySelector('div[id="searchResults"]');


function changeView(element, condition){
    !condition ? element.style.cssText = 'display:none' : element.style.cssText = 'display:block'
}

function changeView2(element, condition){
    !condition ? element.style.cssText = 'display:none' : element.style.cssText = 'display:flex'
}

changeView(areNotUniformView, false);

let indexAreUniform = 0;
let indexReportArea = 0;

function changeAreUniformView(){
    if(indexAreUniform % 2 === 0){
        changeView(areUniformView, false);
        changeView(areNotUniformView, true);
    }else{
        changeView(areUniformView, true);
        changeView(areNotUniformView, false);
    }
    indexAreUniform++;
}

function showReportArea(){
    if(indexReportArea % 2 === 0){
        changeView2(reportPanel, true);
    }else{
        changeView2(reportPanel, false);
        search.setAttribute("value", "");
    }
    indexReportArea++;
}

function hideReportArea(){
    changeView2(reportPanel, false);
    indexReportArea = 0;
}

search.addEventListener("keyup", function (event){
    if(event.key === "Enter"){
        // event.preventDefault();
        const data = {search: this.value};

        fetch("/search", {
            method: "POST",
            headers:{
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        }).then(function (response){
            return response.json();
        }).then(function (busStops){
            searchResults.innerHTML = "";
            loadBusStops(busStops)
        });
    }
});

function loadBusStops(busStops){
    busStops.forEach(busStop => {
        createBusStop(busStop);
    });
}

function createBusStop(busStop){
    const template = document.querySelector("#busStopsTemplate");

    const clone = template.content.cloneNode(true);

    const button = clone.querySelector("button");
    button.innerHTML = busStop.title;
    button.setAttribute("id", busStop.title);
    button.setAttribute("onclick", "setSearchbarUsingBusStopTitle(this.id)");

    searchResults.appendChild(clone);
}

function setSearchbarUsingBusStopTitle(title){
    search.setAttribute("value", title);
}




