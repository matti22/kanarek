const form = document.querySelector("form");
const nicknameInput = form.querySelector('input[name="nickname"]');
const emailInput = form.querySelector('input[name="email"]');
const confirmedPasswordInput = form.querySelector('input[name="password-repeat"]');
const passwordInput = form.querySelector('input[name="password"]');

function isEmail(email){
    return /\S+@\S+\.\S+/.test(email);
}

function arePasswordsSame(password, passwordRepeat){
    return password === passwordRepeat;
}

function itIsLongEnough(password){
    return password.length >= 8;
}

function itIsTooLong(nickname){
    return nickname.length <= 20;
}

function markValidation(element, condition){
    !condition ? element.classList.add('no-valid') : element.classList.remove('no-valid');
}

function validateEmail() {
    setTimeout(function () {
            markValidation(emailInput, isEmail(emailInput.value));
        },
        1000
    );
}

function validatePassword() {
    setTimeout( function (){
            const condition = arePasswordsSame(
                passwordInput.value,
                confirmedPasswordInput.value
            );
            markValidation(confirmedPasswordInput, condition);
        },
        1000
    );
}

function validateSizePassword(){
    setTimeout(function (){
        markValidation(passwordInput, itIsLongEnough(passwordInput.value));
    },
        1000
    );
}

function validateSizeNickname(){
    setTimeout(function (){
        markValidation(nicknameInput, itIsTooLong(nicknameInput.value));
    },
        1000
    );
}

nicknameInput.addEventListener('keyup', validateSizeNickname);
emailInput.addEventListener('keyup',validateEmail);
passwordInput.addEventListener('keyup', validateSizePassword);
confirmedPasswordInput.addEventListener('keyup', validatePassword);

